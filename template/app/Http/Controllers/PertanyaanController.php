<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
class PertanyaanController extends Controller
{
    //
    public function create(){
        return view('pertanyaan.create');
    }

    public function store(Request $request){
        // dd($request->all()); //untuk menampilkan data sementara
        //validasi request untuk tampilan error
        $request->validate([
            'judul' => 'required|unique:pertanyaan',
            'isi'   => 'required'
        ]);
        $query = DB::table('pertanyaan')->insert([
            'judul' => $request ['judul'],
            'isi'   => $request ['isi']
        ]);
        //$query untuk menampilkan data dari database denggan tabel pertanyaan
        return redirect('/pertanyaan')->with('success', 'pertanyaan berhasil disimpan');
    }

    public function index(){
        $pertanyaan = DB::table('pertanyaan')->get(); //mengambil data daro database
        return view('pertanyaan.index', compact('pertanyaan'));
    }
    //menampilan data dari i tertentu
    public function show($id){
        $post = DB::table('pertanyaan')->where('id', $id)->first();
        return view('pertanyaan.show', compact('post'));
    }

    public function edit($id){
        $post = DB::table('pertanyaan')->where('id', $id)->first();

        return view('pertanyaan.edit', compact('post'));

    }

    public function update($id, Request $request){
        $request->validate([
            'judul' => 'required|unique:pertanyaan',
            'isi'   => 'required'
        ]);

        $query = DB::table('pertanyaan')
                    ->where('id', $id)
                    ->update([
                        'judul' => $request['judul'],
                        'isi'   => $request['isi']
                    ]);

        return redirect('/pertanyaan')->with('success', 'berhasil edit pertanyaan');
    }

    public function destroy($id){
        $query = DB::table('pertanyaan')->where('id', $id)->delete();
        return redirect('/pertanyaan')->with('success', 'Pertanyaan berhasil di hapus');
    }
}
