<!DOCTYPE html>
<html>
    <head>
        <title> Form Pendaftaran</title>
    </head>

    <body>
        <h2> Buat Account Baru! </h2>
        <h3> Sign Up Form </h3>

        <div>
            <form action="/welcome" method="POST">
            @csrf
                <label for="nama_depan"> Fisrt Name : </label> <br> <br>
                <input  type="text" name="firstname" id="firstname"> <br> <br>
                <label for="nama_belakang"> Last Name : </label> <br> <br>
                <input type="text" name="lastname" id="lastname"> <br> <br>

                <label> Gender : </label> <br> <br>
                <input type="radio" name="jenis_kelamin" value="0"> Male <br>
                <input type="radio" name="jenis_kelamin" value="1"> Female <br>
                <input type="radio" name="jenis_kelamin" value="2"> Other <br> <br>
                    
                <label> Nationality</label> <br> <br>
                <select>
                    <option value="indonesia"> Indonesia</option>
                    <option value="Singapura"> Singapura</option>
                    <option value="Malaysia"> Malaysia</option>
                </select> <br> <br>

                <label> Language Spoken : </label> <br> <br>
                <input type="checkbox" name="bahasa" value="0"> Bahasa Indonesia <br>
                <input type="checkbox" name="bahasa" value="1"> English <br>
                <input type="checkbox" name="bahasa" value="2"> Other <br> <br>

                <label for="biodata"> Bio : </label> <br> <br>
                <textarea cols="100" rows="7" id="biodata"></textarea> <br>

                <input type="submit" value="Sign Up"> 

            </form>
        </div> 

    </body>
</html>